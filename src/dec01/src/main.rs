use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashSet;

fn main() {
    let f = File::open("input/1.txt").unwrap();
    let file = BufReader::new(&f);
    let mut values = Vec::new();

    for line in file.lines() {
        let value = line.unwrap().parse::<i32>().unwrap();
        values.push(value)
    }

    // First part.
    let mut visited = HashSet::new();

    for value in &values {
        let delta = 2020 - value;

        if visited.contains(&delta) {
            println!("Answer (1): {}", delta * value);
            break;
        }

        visited.insert(value);
    }

    // Second part: very naive.
    for a in 0 .. values.len() - 2 {
        for b in a + 1 .. values.len() - 1 {
            for c in b + 1 .. values.len() {
                if values[a] + values[b] + values[c] == 2020 {
                    println!("Answer (2): {}", values[a] * values[b] * values[c]);
                    break;
                }
            }
        }
    }
}
