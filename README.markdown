# Advent of Code 2020

In this repository I track my Rust solutions to the Advent of Code 2020 advent
calendar.

For more information have a look at https://adventofcode.com/2020/
